package it.patrizio.NientePostiInPiedi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NientePostiInPiediApplication {

	public static void main(String[] args) {
		SpringApplication.run(NientePostiInPiediApplication.class, args);
	}

}
