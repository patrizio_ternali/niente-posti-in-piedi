package it.patrizio.NientePostiInPiedi.controller;

import it.patrizio.NientePostiInPiedi.entity.User;
import it.patrizio.NientePostiInPiedi.entity.Room;
import it.patrizio.NientePostiInPiedi.service.RoomService;
import it.patrizio.NientePostiInPiedi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prenotastanza")
public class PrenotaStanzaController {

    private final RoomService roomService;
    private final UserService userService;

    @Autowired
    public PrenotaStanzaController(RoomService roomService, UserService userService) {
        this.roomService = roomService;
        this.userService = userService;
    }

    @PostMapping("/{idStanza}/{userId}")
    public String joinRoom(@PathVariable("idStanza") Integer idStanza,
                           @PathVariable("userId") Integer idUser) throws Exception {
        if (idStanza == null || idUser == null) {
            return "Inserisci tutti i campi";
        } else {
            if (userService.findById(idUser) != null && roomService.findById(idStanza) != null) {
                User user = userService.findById(idUser);
                Room room = roomService.findById(idStanza);
                roomService.inserisciUser(user,room);
            }
            return "Utente inserito correttamente nella stanza";
        }
    }

}
