package it.patrizio.NientePostiInPiedi.controller;

import it.patrizio.NientePostiInPiedi.request.LoginRequest;
import it.patrizio.NientePostiInPiedi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/log")
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login(@RequestBody LoginRequest loginRequest) {
        if (userService.findByEmailAndPassword(loginRequest.getEmail(), loginRequest.getPassword()) != null) {
            return "Login eseguito correttamente";
        }
        return "Errore durante il login";
    }

}
