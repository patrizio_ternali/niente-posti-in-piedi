package it.patrizio.NientePostiInPiedi.controller;

import it.patrizio.NientePostiInPiedi.entity.Tipo;
import it.patrizio.NientePostiInPiedi.request.PersonaRequest;
import it.patrizio.NientePostiInPiedi.entity.User;
import it.patrizio.NientePostiInPiedi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reg")
public class RegisterController {

    private final UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public String register(@RequestBody PersonaRequest persona) {
        if (persona.getTipo().equalsIgnoreCase("admin")) {
            Tipo tipo = Tipo.ADMIN;
            userService.save(new User(persona.getNome(),
                    persona.getCognome(), persona.getEmail(), persona.getPassword(), tipo));
            return "Admin salvato correttamente.";
        } else if(persona.getTipo().equalsIgnoreCase("user")){
            Tipo tipo = Tipo.USER;
            userService.save(new User(persona.getNome(),
                    persona.getCognome(), persona.getEmail(), persona.getPassword(), tipo));
            return "Utente aggiunto correttamente";
        }
        return "null";

    }

}
