package it.patrizio.NientePostiInPiedi.controller;

import it.patrizio.NientePostiInPiedi.entity.Tipo;
import it.patrizio.NientePostiInPiedi.entity.User;
import it.patrizio.NientePostiInPiedi.request.CreateRoomRequest;
import it.patrizio.NientePostiInPiedi.entity.Room;
import it.patrizio.NientePostiInPiedi.service.RoomService;
import it.patrizio.NientePostiInPiedi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    private final RoomService roomService;
    private final UserService userService;

    @Autowired
    public RoomController(RoomService roomService, UserService userService) {
        this.roomService = roomService;
        this.userService = userService;
    }

    @GetMapping("/listroom")
    public List<Room> getRooms() {
        List<Room> rooms = roomService.findAll();
        for (Room room : rooms) {
            room.setPostiMassimi(room.getPostiIniziali() - room.getUsers().size());
        }
        return rooms;
    }

    @DeleteMapping("/deleteuser/{id}")
    public String deleteUser(@PathVariable int id) {
        User user = userService.findById(id);
        if (user.getTipo().equals(Tipo.ADMIN)) {
            return "Non puoi eliminare un admin!";
        } else {
            userService.deleteById(id);
            return "User eliminato correttamente";
        }
    }

    @PostMapping("/createroom")
    public String creaStanza(@RequestBody CreateRoomRequest request) {
        User user = userService.findById(request.getId_admin());
        if (user != null) {
            if (user.getTipo().equals(Tipo.ADMIN)) {
                Room room = Room.creaStanza(request.getNome_stanza(), request.getPosti_massimi());
                roomService.save(room);
                return "Stanza creata";
            } else {
                return "Soltanto gli admin possono creare stanze";
            }
        }
        return "Nessun admin trovato con queste credenziali";
    }

}
