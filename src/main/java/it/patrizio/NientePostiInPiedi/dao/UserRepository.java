package it.patrizio.NientePostiInPiedi.dao;

import it.patrizio.NientePostiInPiedi.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmailAndPassword(String email, String password);
    User findById(int id);
    boolean deleteById(int id);

}
