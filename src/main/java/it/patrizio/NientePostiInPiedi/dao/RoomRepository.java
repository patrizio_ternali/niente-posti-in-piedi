package it.patrizio.NientePostiInPiedi.dao;

import it.patrizio.NientePostiInPiedi.entity.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

    Room findById(int id);
    List<Room> findAll();

}
