package it.patrizio.NientePostiInPiedi.request;

public class PersonaRequest {

    private String nome;
    private String cognome;
    private String email;
    private String password;
    private String tipo;

    public PersonaRequest(String nome, String cognome, String email, String password, String tipo) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
        this.tipo = tipo;
    }

    public PersonaRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo() {return this.tipo;}

    public void setTipo(String tipo) {this.tipo = tipo;}
}
