package it.patrizio.NientePostiInPiedi.request;

public class CreateRoomRequest {

    private int id_admin;
    private String nome_stanza;
    private int posti_massimi;

    public CreateRoomRequest(int id_admin, String nome, int postiMassimi) {
        this.id_admin = id_admin;
        this.nome_stanza = nome;
        this.posti_massimi = postiMassimi;
    }

    public int getId_admin() {
        return id_admin;
    }

    public void setId_admin(int id_admin) {
        this.id_admin = id_admin;
    }

    public String getNome_stanza() {
        return nome_stanza;
    }

    public void setNome_stanza(String nome_stanza) {
        this.nome_stanza = nome_stanza;
    }

    public int getPosti_massimi() {
        return posti_massimi;
    }

    public void setPosti_massimi(int posti_massimi) {
        this.posti_massimi = posti_massimi;
    }
}
