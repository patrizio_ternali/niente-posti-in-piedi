package it.patrizio.NientePostiInPiedi.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrooms")
    private int id;
    @Column(name = "nome_stanza")
    private String nomeStanza;
    @Column(name = "posti_stanza")
    private int postiMassimi;
    @OneToMany(mappedBy = "room")
    private List<User> users;

    private int postiIniziali;


    public Room() {}

    public Room(String nomeStanza, int postiMassimi) {
        this.nomeStanza = nomeStanza;
        this.postiMassimi = postiMassimi;
        this.postiIniziali = postiMassimi;
    }

    public static Room creaStanza(String nome, int postiMassimi) {
        return new Room(nome,postiMassimi);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeStanza() {
        return nomeStanza;
    }

    public void setNomeStanza(String nomeStanza) {
        this.nomeStanza = nomeStanza;
    }

    public int getPostiMassimi() {
        return postiMassimi;
    }

    public void setPostiMassimi(int postiMassimi) {
        this.postiMassimi = postiMassimi;
    }

    public List<User> getUsers() {
        return this.users;
    }

    public int getPostiIniziali() {return this.postiIniziali;}

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
