package it.patrizio.NientePostiInPiedi.service;
import it.patrizio.NientePostiInPiedi.dao.RoomRepository;
import it.patrizio.NientePostiInPiedi.entity.User;
import it.patrizio.NientePostiInPiedi.entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final UserService userService;

    @Autowired
    public RoomService(RoomRepository roomRepository, UserService userService) {
        this.roomRepository = roomRepository;
        this.userService = userService;
    }

    public void save(Room room) {
        roomRepository.save(room);
    }

    public Room findById(int id) {
        return roomRepository.findById(id);
    }

    public void inserisciUser(User user, Room room) throws Exception {
        if (room.getPostiMassimi() == 0) {
            throw new Exception();
        }
        room.setPostiMassimi(room.getPostiMassimi()-1);
        user.setRoom(room);
        userService.save(user);
    }

    public List<Room> findAll() {
        return roomRepository.findAll();
    }

}
