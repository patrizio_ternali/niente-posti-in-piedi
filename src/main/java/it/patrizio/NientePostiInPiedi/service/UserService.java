package it.patrizio.NientePostiInPiedi.service;

import it.patrizio.NientePostiInPiedi.dao.UserRepository;
import it.patrizio.NientePostiInPiedi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email,password);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public User findById(int id) {
        return userRepository.findById(id);
    }

    public boolean deleteById(int id) {
        return userRepository.deleteById(id);
    }

}
