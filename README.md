# Niente Posti In Piedi

progetto per esercitazione di gruppo con spring boot (base)

## Getting started

Si vuole realizzare un programma tramite spring framework che gestisca i posti a sedere in una stanza.
Il programma deve avere le seguente caratteristiche:


Avere un profilazione degli utenti:

- Admin
- User 

L'utente Admin può:

- Creare Stanze
- Approvare utenti (facoltativo)


L'utente User può:

- Accedere alla piattafomra
- Selezionare la stanza di interesse
- prenotare un posto


### nota
La descrizione del progetto è volutamente scarna per vedere il livello di autonomia dell'alunno
